package main

import (
	"fmt"
	"./lexer"
)

func main() {
	fmt.Println(lexer.Lexer("(car (cdr (cons 1 (cons 2 (cons 3 nil)))))"))
	fmt.Println(lexer.Lexer("(car (cdr (1 . (2 . (3 . nil)))))"))
}
