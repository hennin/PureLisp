package combinator

type SourceReader struct {
	src string
	// 何文字目か(改行文字含む)
	pos int
	// 行数
	line int
	// (改行した位置から)何文字目か
	col int
}

type Tokenizer func(*SourceReader) (string, error)


