package combinator

import "errors"

// 自身のトークナイズが失敗した場合、引数のトークナイザーを実行するトークナイザーを返す
func (tknz Tokenizer) Or(tknz1 Tokenizer) Tokenizer {
	return func(sr *SourceReader) (string, error) {
		var bak = sr.Clone()
		if str, err := tknz(sr); err == nil {
			return str, err
		} else if *sr != bak {
			return "", err
		}
		if str, err := tknz1(sr); err == nil {
			return str, err
		} else {
			return "", err
		}
	}
}

// 例外を発生させるトークナイザーを返す
func (tknz Tokenizer) Left(errMsg string) Tokenizer {
	return tknz.Or(func(sr *SourceReader) (string, error) {
		return "", errors.New(sr.Ex(errMsg))
	})
}

// 自身と引数のトークナイザーを実行し、引数のトークナイズ結果を無視し自身の結果を返すトークナイザーを返す
func (tknz Tokenizer) Prev(tknz1 Tokenizer) Tokenizer {
	return func(sr *SourceReader) (string, error) {
		if str, err := tknz(sr); err != nil {
			return "", err
		} else if _, err := tknz1(sr); err != nil {
			return "", err
		} else {
			return str, err
		}
	}
}

// 自身と引数のトークナイザーを実行し、自身のトークナイズ結果を無視し引数の結果を返すトークナイザーを返す
func (tknz Tokenizer) Next(tknz1 Tokenizer) Tokenizer {
	return func(sr *SourceReader) (string, error) {
		if _, err := tknz(sr); err != nil {
			return "", err
		} else if str, err := tknz1(sr); err != nil {
			return "", err
		} else {
			return str, err
		}
	}
}

// 自身のトークナイズに失敗しても無視して引数のトークナイザーを実行し、トークナイズ結果を結合したものを返すトークナイザーを返す
func (tknz Tokenizer) Option(tknz1 Tokenizer) Tokenizer {
	return func(sr *SourceReader) (string, error) {
		var str, _ = tknz(sr)
		if str1, err := tknz1(sr); err == nil {
			return str + str1, err
		} else {
			return "", err
		}
	}
}

// 引数のトークナイズの結果を無視し、さらにSourceReaderへの変更も無視し、自身のトークナイズ結果を返すトークナイザーを返す
func (tknz Tokenizer) Ignore(tknz1 Tokenizer) Tokenizer {
	return func(sr *SourceReader) (string, error) {
		var ret = ""
		if str, err := tknz(sr); err != nil {
			return "", err
		} else {
			ret += str
		}
		var bak = sr.Clone()
		if _, err := tknz1(sr); err != nil {
			return "", err
		}
		sr.Revert(bak)
		return ret, nil
	}
}
