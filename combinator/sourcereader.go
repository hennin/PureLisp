package combinator

import (
	"errors"
	"strconv"
)

func GetSourceReader(src string) *SourceReader {
	return &SourceReader{src, 0, 1,1}
}

func (sr *SourceReader) Peek() (rune, error) {
	if sr.pos >= len(sr.src) {
		return 0, errors.New("Already reach the end of string.")
	}
	return rune(sr.src[sr.pos]), nil
}

func (sr *SourceReader) Next() {
	if chr, err := sr.Peek(); err == nil && chr == '\n' {
		sr.line++
		sr.col = 0
	}
	sr.pos++
	sr.col++
}

func (sr SourceReader) Clone() SourceReader {
	return sr
}

// バックトラック
func (sr *SourceReader) Revert(bak SourceReader) error {
	if sr.src != bak.src {
		return errors.New("can not revert")
	}
	sr.pos = bak.pos
	sr.line = bak.line
	sr.col = bak.col
	return nil
}

// エラーメッセージのカスタマイズ
func (sr *SourceReader) Ex(errMsg string) string {
	var ret = "[line " + strconv.Itoa(sr.line) + ", col " + strconv.Itoa(sr.col) + "] " + errMsg
	if chr, err := sr.Peek(); err == nil {
		ret += ": at '" + string(chr) + "'"
	}
	return ret
}
