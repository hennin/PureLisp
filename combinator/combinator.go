package combinator

import (
	"fmt"
	"errors"
	"unicode"
	"strconv"
)

// トークナイザーのテスト用関数
func TokenizeTest(tknz Tokenizer, src string) {
	var sr = GetSourceReader(src)
	if str, err := tknz(sr); err == nil {
		fmt.Println(str)
	} else {
		fmt.Println(err)
	}
}

// 条件を満たす1文字をトークナイズするトークナイザーを返す
func Satisfy(fn func(rune) bool) Tokenizer {
	return func(sr *SourceReader) (string, error) {
		if chr, err := sr.Peek(); err != nil {
			return "", err
		} else if fn(chr) {
			sr.Next()
			return string(chr), err
		} else {
			return "", errors.New(sr.Ex("not satisfy"))
		}
	}
}

// いずれか1文字をトークナイズするトークナイザーを返す
var AnyChar = Satisfy(func(chr rune) bool { return true })

// 任意の1文字をトークナイズするトークナイザーを返す
func Char1(chr rune) Tokenizer {
	return Satisfy(func(c rune) bool { return c == chr }).Left("not satisfy")
}

func isAlphaNum(chr rune) bool {
	return unicode.IsLetter(chr) || unicode.IsDigit(chr)
}

var Digit = Satisfy(unicode.IsDigit).Left("not digit")

var Upper = Satisfy(unicode.IsUpper).Left("not upper")

var Lower = Satisfy(unicode.IsLower).Left("not lower")

var Alpha = Satisfy(unicode.IsLetter).Left("not alpha")

var AlphaNum = Satisfy(isAlphaNum).Left("not alphaNum")

func isSpace(chr rune) bool {
	return chr == '\t' || chr == ' '
}

// 半角スペースまたはタブ文字をトークナイズするトークナイザーを返す
var Space = Satisfy(isSpace).Left("not space")

// 半角スペースまたはタブ文字を複数トークナイズするトークナイザーを返す
var Spaces = Many(Space)

// 受け取ったトークナイザーを順に実行し、トークナイズ結果を結合したものを返すトークナイザーを返す
func Sequence(tknzs ...Tokenizer) Tokenizer {
	return func(sr *SourceReader) (string, error) {
		var ret = ""
		for _, tknz := range tknzs {
			if str, err := tknz(sr); err == nil {
				ret += str
			} else {
				return ret, err
			}
		}
		return ret, nil
	}
}

// 受け取ったトークナイザーをn回繰り返すトークナイザーを返す (and結合)
func Replicate(n int, tknz Tokenizer) Tokenizer {
	return func(sr *SourceReader) (string, error) {
		var ret = ""
		for i := 0; i < n; i++ {
			if str, err := tknz(sr); err == nil {
				ret += str
			} else {
				return ret, err
			}
		}
		return ret, nil
	}
}

// 受け取ったトークナイザーを失敗するまで0回以上繰り返すトークナイザーを返す (and結合)
func Many(tknz Tokenizer) Tokenizer {
	return func(sr *SourceReader) (string, error) {
		var ret = ""
		for ;; {
			if str, err := tknz(sr); err == nil {
				ret += str
			} else {
				break
			}
		}
		return ret, nil
	}
}

// 受け取ったトークナイザーを失敗するまで1回以上繰り返すトークナイザーを返す (and結合)
func Many1(tknz Tokenizer) Tokenizer {
	return func(sr *SourceReader) (string ,error) {
		var ret = ""
		if str, err := tknz(sr); err == nil {
			ret += str
		} else {
			return "", err
		}
		if str, err := Many(tknz)(sr); err == nil {
			ret += str
		}
		return ret, nil
	}
}

// 受け取ったトークナイザーを順に実行し、最初に成功した結果を返すトークナイザーを返す (or結合)
func Or(tknzs ...Tokenizer) Tokenizer {
	return func(sr *SourceReader) (string, error) {
		var error = errors.New("empty or")
		for _, tknz := range tknzs {
			if str, err := tknz(sr); err == nil {
				return string(str), err
			} else {
				error = err
			}
		}
		return "", error
	}
}

// バックトラック
func Tryp(tknz Tokenizer) Tokenizer {
	return func(sr *SourceReader) (string, error) {
		var bak = sr.Clone()
		if str, err := tknz(sr); err != nil {
			sr.Revert(bak)
			return "", err
		} else {
			return str, err
		}
	}
}

// 任意の文字列をトークナイズするトークナイザーを返す
func String(str string) Tokenizer {
	return func(sr *SourceReader) (string, error) {
		for chr := range str {
			Char1(rune(chr)).Left("not string \"" + str + "\"")(sr)
		}
		return str, nil
	}
}

// エラーを発生させるトークナイザーを返す
func Left(errMsg string) Tokenizer {
	return func(sr *SourceReader) (string, error) {
		return "", errors.New(sr.Ex(errMsg))
	}
}

// トークナイズした結果に適用する関数を設定したトークナイザーを返す
func Apply(fn func(string) (string, error), tknz Tokenizer) Tokenizer {
	return func(sr *SourceReader) (string, error) {
		if str, err := tknz(sr); err == nil {
			if str, err := fn(str); err == nil {
				return str, err
			} else {
				return "", err
			}
		} else {
			return "", err
		}
	}
}

// トークナイズした結果が数値の場合、その数値の符号を反転させる関数を設定したトークナイザーを返す
func Neg(tknz Tokenizer) Tokenizer {
	return Apply(func(str string) (string, error) {
		if number, err := strconv.Atoi(str); err != nil {
			return "", errors.New("must be digit")
		} else {
			return strconv.Itoa(- number), err
		}
	}, tknz)
}
