PureLisp
========

Go言語による[純Lisp](https://ja.wikipedia.org/wiki/%E7%B4%94LISP)のインタプリタの実装


## Description
字句解析、構文解析のあとそのままGo言語で処理する

インタプリタというよりただのパーサ


### DONE
- パーサコンビネータ
- 字句解析

### DONE
- 構文解析


## Usage
```bash
$ cd $GOPATH
$ git clone https://gitlab.com/hennin/PureLisp.git
$ cd PureLisp
$ go run main.go
```


## Reference

[Java 再帰下降構文解析 超入門](http://qiita.com/7shi/items/64261a67081d49f941e3#_reference-8ba8d52f896fdda3a7da)


## Licence

[MIT](https://github.com/tcnksm/tool/blob/master/LICENCE)


## Author

[hennin](https://gitlab.com/hennin)
