package lexer

import (
	"errors"
	"strconv"
)

func (ts TokenStream) String() string {
	var ret = "TokenStream{stream:   ["
	for _, t := range ts.stream {
		ret += t.String() + "\n                       "
	}
	return ret[0:len(ret) - 1] + "],\n            curIndex: " + strconv.Itoa(ts.curIndex) + "}"
}

func (ts *TokenStream) pushToken(t Token) {
	ts.stream = append(ts.stream, t)
}

func (ts *TokenStream) GetCurIndex() int {
	return ts.curIndex
}

func (ts *TokenStream) GetToken() Token {
	return ts.stream[ts.curIndex]
}

func (ts *TokenStream) UngetToken(times int) error {
	for i := 0; i < times; i++ {
		if ts.curIndex == 0 {
			return errors.New("Can't revert beyond 0")
		} else {
			ts.curIndex--
		}
	}
	return nil
}
func (ts *TokenStream) GetNextToken() (Token, error) {
	if ts.curIndex >= len(ts.stream) - 1 {
		return Token{}, errors.New("Current token is the last token.")
	}
	ts.curIndex++
	return ts.GetToken(), nil
}

func (ts *TokenStream) GetCurType() TokenType {
	return ts.GetToken().tokenType
}

func (ts *TokenStream) GetCurStr() string {
	return ts.GetToken().tokenStr
}

func (ts *TokenStream) GetCurNumber() (int, error) {
	if ts.GetToken().tokenType != NUMBER {
		return 0, errors.New("This token is not number.")
	}
	return ts.GetToken().number, nil
}
