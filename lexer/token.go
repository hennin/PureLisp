package lexer

import (
	"strconv"
	"fmt"
)

func (t Token) String() string {
	return "Token{tokenType: " + fmt.Sprintf("%-6s", t.tokenType.String())  + " , tokenStr: "+
		fmt.Sprintf("%-6s", t.tokenStr) + " , number: " + strconv.Itoa(t.number) + "}"
}
