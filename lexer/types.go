package lexer

import "../combinator"

type TokenType int

const (
	LPAR TokenType = iota
	RPAR
	DOT
	NUMBER
	SYMBOL
)

type Token struct {
	tokenType TokenType
	tokenStr string
	number int
}

type TokenStream struct {
	stream []Token
	curIndex int
}

type Parser func(*combinator.SourceReader) (Token, error)
