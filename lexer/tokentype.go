package lexer

func (tt TokenType) String() string {
	switch tt {
	case LPAR:
		return "LPAR"
	case RPAR:
		return "RPAR"
	case DOT:
		return "DOT"
	case NUMBER:
		return "NUMBER"
	case SYMBOL:
		return "SYMBOL"
	default:
		return "Unknown"
	}
}
