package lexer

import (
	"../combinator"
	"strconv"
	"errors"
	"fmt"
)

var lpar = combinator.Tryp(combinator.Char1('(')).Left("not left parenthesis")
var rpar = combinator.Tryp(combinator.Char1(')')).Left("not right parenthesis")
var dot = combinator.Tryp(combinator.Char1('.')).Left("not dot")

func delimiter(sr *combinator.SourceReader) (string, error) {
	// 文字列の最後も区切り文字と見なす
	if _, err := sr.Peek(); err != nil {
		return "", nil
	}
	return combinator.Tryp(combinator.Or(lpar, rpar, combinator.Many1(combinator.Space), combinator.Char1('\n'))).Left("not delimiter")(sr)
}

var number = combinator.Tryp(combinator.Many(combinator.Digit)).Ignore(delimiter).Left("not number")
var symbol = combinator.Tryp(combinator.Sequence(combinator.Alpha, combinator.Many(combinator.AlphaNum)).Ignore(delimiter)).Left("not symbol")

func parseLpar(sr *combinator.SourceReader) (Token, error) {
	combinator.Spaces(sr)
	if str, err := lpar(sr); err != nil {
		return Token{}, err
	} else {
		return Token{tokenType: LPAR, tokenStr: str}, err
	}
}

func parseRpar(sr *combinator.SourceReader) (Token, error) {
	combinator.Spaces(sr)
	if str, err := rpar(sr); err != nil {
		return Token{}, err
	} else {
		return Token{tokenType: RPAR, tokenStr: str}, err
	}
}

func parseDot(sr *combinator.SourceReader) (Token, error) {
	combinator.Spaces(sr)
	if str, err := dot(sr); err != nil {
		return Token{}, err
	} else {
		return Token{tokenType: DOT, tokenStr: str}, err
	}
}

func parseNumber(sr *combinator.SourceReader) (Token, error) {
	combinator.Spaces(sr)
	if str, err := number(sr); err != nil {
		return Token{}, err
	} else if number, err :=  strconv.Atoi(str); err != nil {
		return Token{}, err
	} else {
		return Token{tokenType: NUMBER, tokenStr: str, number: number}, err
	}
}

func parseSymbol(sr *combinator.SourceReader) (Token, error) {
	combinator.Spaces(sr)
	if str, err := symbol(sr); err != nil {
		return Token{}, err
	} else {
		return Token{tokenType: SYMBOL, tokenStr: str}, err
	}
}

func parseOr(prsrs ...Parser) Parser {
	return func(sr *combinator.SourceReader) (Token, error) {
		var error = errors.New("empty or")
		for _, prsr := range prsrs {
			if t, err := prsr(sr); err == nil {
				return t, nil
			} else {
				error = err
			}
	    }
		return Token{}, error
	}
}

func ParseTest(src string) {
	fmt.Println("src: ", src)
	for _, prsr := range []Parser{parseLpar, parseRpar, parseDot, parseNumber, parseSymbol} {
		var sr = combinator.GetSourceReader(src)
		if t, err := prsr(sr); err == nil {
			fmt.Println(t)
		} else {
			fmt.Println(err)
		}
		fmt.Println()
	}
}

func Lexer(src string) (TokenStream, error) {
	var sr = combinator.GetSourceReader(src)
	var ts = TokenStream{}

	for ;; {
		if _, err := sr.Peek(); err != nil {
			break
		} else if t, err := parseOr(parseLpar, parseRpar, parseDot, parseNumber, parseSymbol)(sr); err == nil {
			ts.pushToken(t)
		} else {
			return ts, err
		}
	}

	return ts, nil
}
